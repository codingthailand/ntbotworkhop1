﻿using Microsoft.Bot.Builder;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.Bot.Builder.Dialogs;
using System.Net.Http;

namespace NTBotWorkshop1.Dialogs
{
    public class MainDialog : LogoutDialog
    {
        protected readonly ILogger Logger;

        public MainDialog(IConfiguration configuration, ILogger<MainDialog> logger)
            : base(nameof(MainDialog), configuration["ConnectionName"]!)
        {
            Logger = logger;

            AddDialog(new OAuthPrompt(
                nameof(OAuthPrompt),
                new OAuthPromptSettings
                {
                    ConnectionName = ConnectionName,
                    Text = "กรุณาล็อกอินก่อนเพื่อใช้งาน Bot",
                    Title = "Log In",
                    Timeout = 300000, // User has 5 minutes to login (1000 * 60 * 5)
                }));

            AddDialog(new ConfirmPrompt(nameof(ConfirmPrompt)));

            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                PromptStepAsync,
                LoginStepAsync,
            }));

            // The initial child Dialog to run.
            InitialDialogId = nameof(WaterfallDialog);
        }

        private async Task<DialogTurnResult> PromptStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            return await stepContext.BeginDialogAsync(nameof(OAuthPrompt), null, cancellationToken);
        }

        private async Task<DialogTurnResult> LoginStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            // Get the token from the previous step. Note that we could also have gotten the
            // token directly from the prompt itself. There is an example of this in the next method.
            var tokenResponse = (TokenResponse)stepContext.Result;
            if (tokenResponse != null)
            {
                // ล็อกอินสำเร็จ
                await stepContext.Context.SendActivityAsync(MessageFactory.Text("You are now logged in."), cancellationToken);

                await stepContext.Context.SendActivityAsync(MessageFactory.Text($"You token is {tokenResponse.Token}"), cancellationToken);

                // Replace with the user's access token obtained from the Google OAuth flow
                string userAccessToken = tokenResponse.Token;

                // Create an HTTP client and set the authorization header
                var httpClient = new HttpClient();
                httpClient.DefaultRequestHeaders.Add("Authorization", $"Bearer {userAccessToken}");

                // Make a request to the Google People API to get the user's profile
                HttpResponseMessage response = await httpClient.GetAsync("https://people.googleapis.com/v1/people/me?personFields=names,emailAddresses", cancellationToken);

                if (response.IsSuccessStatusCode)
                {
                    // Parse the user's profile information from the response
                    var userProfile = await response.Content.ReadAsStringAsync(cancellationToken);

                    // Display the user's profile information to the user
                    var message = MessageFactory.Text($"Here is your Google profile information:\n\n{userProfile}");
                    await stepContext.Context.SendActivityAsync(message, cancellationToken);
                }
                else
                {
                    // Handle the error if the API request is not successful
                    var errorMessage = $"Failed to retrieve the user's Google profile. Status code: {response.StatusCode}";
                    var errorResponse = await response.Content.ReadAsStringAsync();
                    errorMessage += $"\n\nError message: {errorResponse}";

                    var errorResponseMessage = MessageFactory.Text(errorMessage);
                    await stepContext.Context.SendActivityAsync(errorResponseMessage);
                }

            }

            await stepContext.Context.SendActivityAsync(MessageFactory.Text("Login was not successful please try again."), cancellationToken);
            return await stepContext.EndDialogAsync(cancellationToken: cancellationToken);
        }

    }
}
