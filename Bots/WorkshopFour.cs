﻿using Microsoft.Bot.Builder;
using Microsoft.Bot.Schema;
using Newtonsoft.Json;
using NTBotWorkshop1.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text.Json.Nodes;
using System.Threading;
using System.Threading.Tasks;

namespace NTBotWorkshop1.Bots
{
    public class WorkshopFour
    {
        public static async Task HandleActivityAsync(ITurnContext<IMessageActivity> turnContext, CancellationToken cancellationToken)
        {
            var httpClient = new HttpClient();

            var response = await httpClient.GetAsync($"http://api.weatherapi.com/v1/current.json?key=a66aaa298d964a68853145710233110&q={turnContext.Activity.Text}&aqi=no", cancellationToken);

            if (response.IsSuccessStatusCode)
            {
                string responseBody = await response.Content.ReadAsStringAsync(cancellationToken);
                var weather = JsonConvert.DeserializeObject<WeatherData>(responseBody);
                var replyText = MessageFactory.Text($"อากาศตอนนี้ที่ {weather!.Location!.Name} อุณหภูมิ {weather!.Current!.TempC} องศาเซลเซียส");
                await turnContext.SendActivityAsync(replyText, cancellationToken);
            } else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest) // 400
            {
                await turnContext.SendActivityAsync(MessageFactory.Text($"ไม่พบข้อมูลเมือง/ประเทศ {turnContext.Activity.Text} นี้"), cancellationToken);
            }

            await SendSuggestedActionsAsync(turnContext, cancellationToken);

        }


        // welcome text
        public static async Task HandleWelcomeAsync(IList<ChannelAccount> membersAdded, ITurnContext<IConversationUpdateActivity> turnContext, CancellationToken cancellationToken)
        {
            var welcomeText = "ยินดีต้อนรับ นี่คือ NT Bot Workshop 4 ตรวจสอบสภาพอากาศ!";
            foreach (var member in membersAdded)
            {
                if (member.Id != turnContext.Activity.Recipient.Id)
                {
                    await turnContext.SendActivityAsync(MessageFactory.Text(welcomeText, welcomeText), cancellationToken);

                    await SendSuggestedActionsAsync(turnContext, cancellationToken);
                }
            }
        }

        // send suggestedActions
        private static async Task SendSuggestedActionsAsync(ITurnContext turnContext, CancellationToken cancellationToken)
        {
            // var reply = MessageFactory.ContentUrl("https://codingthailand.com/site/img/logo.jpg", "image/jpeg"); // video/mp4 

            //var attachment = new Attachment() { 
            //    Name = "logo",
            //    ContentType = "image/jpeg",
            //    ContentUrl = "https://codingthailand.com/site/img/logo.jpg",
            //    ThumbnailUrl = "https://codingthailand.com/site/img/logo.jpg",
            //    Content = new { width = 120, height = 120 }, // ขึ้นกับ channel/client to support it
            //};

            //var reply = MessageFactory.Attachment(attachment);

            //Card


            //var heroCard = new HeroCard()
            //{
            //    Images = new List<CardImage> { new CardImage("https://codingthailand.com/site/img/book3.png") },
            //    Title = "หนังสือ MS Access",
            //    Subtitle = "เขียนโดย อ.เอก เอกนรินทร์ คำคูณ",
            //    Text = "ไมโครซอฟต์ แอ็คเซส เป็นโปรแกรมประเภทโปรแกรมจัดการฐานข้อมูลเชิงสัมพันธ์ ที่ทำกันในสำนักงาน หรือองค์กรขนาดเล็ก ซึ่งสามารถเก็บข้อมูล ประมวลผลข้อมูล",
            //    Buttons = new List<CardAction> { 
            //        new CardAction(type: ActionTypes.OpenUrl, title: "ดูเพิ่มเติม", value: "https://codingthailand.com"),
            //    }
            //};

            //var reply = MessageFactory.Attachment(heroCard.ToAttachment());

            //Card Carousel

            var heroCard1 = new HeroCard()
            {
                Images = new List<CardImage> { new CardImage("https://codingthailand.com/site/img/book3.png") },
                Title = "หนังสือ MS Access 1",
                Subtitle = "เขียนโดย อ.เอก เอกนรินทร์ คำคูณ",
                Text = "ไมโครซอฟต์ แอ็คเซส เป็นโปรแกรมประเภทโปรแกรมจัดการฐานข้อมูลเชิงสัมพันธ์ ที่ทำกันในสำนักงาน หรือองค์กรขนาดเล็ก ซึ่งสามารถเก็บข้อมูล ประมวลผลข้อมูล",
                Buttons = new List<CardAction> {
                    new CardAction(type: ActionTypes.OpenUrl, title: "ดูเพิ่มเติม", value: "https://codingthailand.com"),
                }
            };

            var heroCard2 = new HeroCard()
            {
                Images = new List<CardImage> { new CardImage("https://codingthailand.com/site/img/book3.png") },
                Title = "หนังสือ MS Access 2",
                Subtitle = "เขียนโดย อ.เอก เอกนรินทร์ คำคูณ",
                Text = "ไมโครซอฟต์ แอ็คเซส เป็นโปรแกรมประเภทโปรแกรมจัดการฐานข้อมูลเชิงสัมพันธ์ ที่ทำกันในสำนักงาน หรือองค์กรขนาดเล็ก ซึ่งสามารถเก็บข้อมูล ประมวลผลข้อมูล",
                Buttons = new List<CardAction> {
                    new CardAction(type: ActionTypes.OpenUrl, title: "ดูเพิ่มเติม", value: "https://codingthailand.com"),
                }
            };

            var heroCard3 = new HeroCard()
            {
                Images = new List<CardImage> { new CardImage("https://codingthailand.com/site/img/book3.png") },
                Title = "หนังสือ MS Access 3",
                Subtitle = "เขียนโดย อ.เอก เอกนรินทร์ คำคูณ",
                Text = "ไมโครซอฟต์ แอ็คเซส เป็นโปรแกรมประเภทโปรแกรมจัดการฐานข้อมูลเชิงสัมพันธ์ ที่ทำกันในสำนักงาน หรือองค์กรขนาดเล็ก ซึ่งสามารถเก็บข้อมูล ประมวลผลข้อมูล",
                Buttons = new List<CardAction> {
                    new CardAction(type: ActionTypes.OpenUrl, title: "ดูเพิ่มเติม", value: "https://codingthailand.com"),
                }
            };

            var heroCard4 = new HeroCard()
            {
                Images = new List<CardImage> { new CardImage("https://codingthailand.com/site/img/book3.png") },
                Title = "หนังสือ MS Access 2",
                Subtitle = "เขียนโดย อ.เอก เอกนรินทร์ คำคูณ",
                Text = "ไมโครซอฟต์ แอ็คเซส เป็นโปรแกรมประเภทโปรแกรมจัดการฐานข้อมูลเชิงสัมพันธ์ ที่ทำกันในสำนักงาน หรือองค์กรขนาดเล็ก ซึ่งสามารถเก็บข้อมูล ประมวลผลข้อมูล",
                Buttons = new List<CardAction> {
                    new CardAction(type: ActionTypes.OpenUrl, title: "ดูเพิ่มเติม", value: "https://codingthailand.com"),
                }
            };


            var heroCards = new List<HeroCard>();
            heroCards.Add(heroCard1);
            heroCards.Add(heroCard2);
            heroCards.Add(heroCard3);
            heroCards.Add(heroCard4);

            var attachments = new List<Attachment>();
            foreach (var card in heroCards)
            {
                attachments.Add(card.ToAttachment());
            }

            var reply = MessageFactory.Carousel(attachments);

            await turnContext.SendActivityAsync(reply, cancellationToken);
        }

    }
}
