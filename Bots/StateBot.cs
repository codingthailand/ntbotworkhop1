﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
//
// Generated with Bot Builder V4 SDK Template for Visual Studio EchoBot v4.18.1

using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Logging;
using NTBotWorkshop1.Models;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace NTBotWorkshop1.Bots
{
    public class StateBot<T> : ActivityHandler where T : Dialog
    {

        protected readonly BotState ConversationState;
        protected readonly Dialog Dialog;
        protected readonly ILogger Logger;
        protected readonly BotState UserState;

        public StateBot(ConversationState conversationState, UserState userState, T dialog, ILogger<StateBot<T>> logger)
        {
            ConversationState = conversationState;
            UserState = userState;
            Dialog = dialog;
            Logger = logger;
        }

        protected override async Task OnMessageActivityAsync(ITurnContext<IMessageActivity> turnContext, CancellationToken cancellationToken)
        {

            // Run the Dialog with the new message Activity.
            //await Dialog.RunAsync(turnContext, ConversationState.CreateProperty<DialogState>(nameof(DialogState)), cancellationToken);

            // Get the state properties from the turn context.

            var conversationStateAccessors = ConversationState.CreateProperty<ConversationData>(nameof(ConversationData));
            var conversationData = await conversationStateAccessors.GetAsync(turnContext, () => new ConversationData(), cancellationToken);

            var userStateAccessors = UserState.CreateProperty<UserProfile>(nameof(UserProfile));
            var userProfile = await userStateAccessors.GetAsync(turnContext, () => new UserProfile(), cancellationToken);

            if (string.IsNullOrEmpty(userProfile.Name))
            {
                // First time around this is set to false, so we will prompt user for name.
                if (conversationData.PromptedUserForName)
                {
                    // Set the name to what the user provided.
                    userProfile.Name = turnContext.Activity.Text?.Trim();

                    // Acknowledge that we got their name.
                    await turnContext.SendActivityAsync($"Hello {userProfile.Name} ต้องการเริ่มต้นแจ้งซ่อมอุปกรณ์เลยมั้ยครับ", cancellationToken: cancellationToken);

                    // Reset the flag to allow the bot to go through the cycle again.
                    conversationData.PromptedUserForName = false;
                }
                else
                {
                    // Prompt the user for their name.
                    await turnContext.SendActivityAsync($"ขอทราบชื่อด้วยครับ?", cancellationToken: cancellationToken);

                    // Set the flag to true, so we don't prompt in the next turn.
                    conversationData.PromptedUserForName = true;
                }
            }
            else
            {
                // BOT START
                await BotStart(conversationData, userProfile, turnContext, cancellationToken);
            }
        }

        private static async Task BotStart(ConversationData flow, UserProfile userProfile, ITurnContext turnContext, CancellationToken cancellationToken)
        {
            var input = turnContext.Activity.Text.Trim();

            switch (flow.LastQuestionAsked)
            {
                case ConversationData.Question.None:
                    await turnContext.SendActivityAsync($"ยินดีต้อนรับคุณ {userProfile.Name} สู่ระบบแจ้งซ่อมอุปกรณ์");
                    await turnContext.SendActivityAsync($"ขอทราบ User Id ของคุณ {userProfile.Name} ด้วยครับ");
                    flow.LastQuestionAsked = ConversationData.Question.UserId;
                    break;

                case ConversationData.Question.UserId:
                    userProfile.UserId = input;
                    await turnContext.SendActivityAsync($"ขอทราบชื่ออุปกรณ์ที่ต้องการซ่อม?");
                    flow.LastQuestionAsked = ConversationData.Question.ToolName;
                    break;

                case ConversationData.Question.ToolName:
                    flow.ToolName = input;
                    var reply = MessageFactory.Text("กรุณาแจ้งและเลือกอาการเสียของอุปกรณ์");
                    reply.SuggestedActions = new SuggestedActions()
                    {
                        Actions = new List<CardAction>()
                        {
                            new CardAction() { Title = "เปิดไม่ติด", Value = "เปิดไม่ติด", Type = ActionTypes.ImBack },
                            new CardAction() { Title = "แอร์ไม่เย็น", Value = "แอร์ไม่เย็น", Type = ActionTypes.ImBack },
                            new CardAction() { Title = "แตกหัก", Value = "แตกหัก", Type = ActionTypes.ImBack },
                        }    
                    };
                    await turnContext.SendActivityAsync(reply, cancellationToken);

                    flow.LastQuestionAsked = ConversationData.Question.ToolDescription;
                    break;

                case ConversationData.Question.ToolDescription:
                    flow.ToolDescription = input;
                    await turnContext.SendActivityAsync($"ขอบคุณสำหรับข้อมูลครับ คุณ {userProfile.Name} {userProfile.UserId}");
                    await turnContext.SendActivityAsync($"แจ้งอุปกรณ์ {flow.ToolName} อาการเสีย {flow.ToolDescription}");
                    // call backend api
                    await turnContext.SendActivityAsync($"กรุณาทัก Bot เพื่อเริ่มการแจ้งซ่อมใหม่");
                    flow.LastQuestionAsked = ConversationData.Question.None;
                    break;

            }

        }

        protected override async Task OnMembersAddedAsync(IList<ChannelAccount> membersAdded, ITurnContext<IConversationUpdateActivity> turnContext, CancellationToken cancellationToken)
        {
            var welcomeText = "ยินดีต้อนรับ นี่คือ NT StateBot!";
            foreach (var member in membersAdded)
            {
                if (member.Id != turnContext.Activity.Recipient.Id)
                {
                    await turnContext.SendActivityAsync(MessageFactory.Text(welcomeText, welcomeText), cancellationToken);
                }
            }
        }


        public override async Task OnTurnAsync(ITurnContext turnContext, CancellationToken cancellationToken = default(CancellationToken))
        {
            await base.OnTurnAsync(turnContext, cancellationToken);

            // Save any state changes that might have occurred during the turn.
            await ConversationState.SaveChangesAsync(turnContext, false, cancellationToken);
            await UserState.SaveChangesAsync(turnContext, false, cancellationToken);
        }




    }
}
