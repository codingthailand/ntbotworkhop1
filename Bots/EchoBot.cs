﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
//
// Generated with Bot Builder V4 SDK Template for Visual Studio EchoBot v4.18.1

using Microsoft.Bot.Builder;
using Microsoft.Bot.Schema;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace NTBotWorkshop1.Bots
{
    public class EchoBot : ActivityHandler
    {
        protected override async Task OnMessageActivityAsync(ITurnContext<IMessageActivity> turnContext, CancellationToken cancellationToken)
        {
            // await WorkshopOne.HandleActivityAsync(turnContext, cancellationToken);
            // await WorkshopTwo.HandleActivityAsync(turnContext, cancellationToken);
            // await WorkshopThree.HandleActivityAsync(turnContext, cancellationToken);
            //await WorkshopFour.HandleActivityAsync(turnContext, cancellationToken);
            await WorkshopFive.HandleActivityAsync(turnContext, cancellationToken);
        }

        protected override async Task OnMembersAddedAsync(IList<ChannelAccount> membersAdded, ITurnContext<IConversationUpdateActivity> turnContext, CancellationToken cancellationToken)
        {
            // await WorkshopOne.HandleWelcomeAsync(membersAdded, turnContext, cancellationToken);
            // await WorkshopTwo.HandleWelcomeAsync(membersAdded, turnContext, cancellationToken);
            //await WorkshopThree.HandleWelcomeAsync(membersAdded, turnContext, cancellationToken);
            //await WorkshopFour.HandleWelcomeAsync(membersAdded, turnContext, cancellationToken);
            await WorkshopFive.HandleWelcomeAsync(membersAdded, turnContext, cancellationToken);
        }
    }
}
