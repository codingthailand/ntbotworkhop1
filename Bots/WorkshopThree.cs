﻿using Microsoft.Bot.Builder;
using Microsoft.Bot.Schema;
using Newtonsoft.Json;
using NTBotWorkshop1.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text.Json.Nodes;
using System.Threading;
using System.Threading.Tasks;

namespace NTBotWorkshop1.Bots
{
    public class WorkshopThree
    {
        public static async Task HandleActivityAsync(ITurnContext<IMessageActivity> turnContext, CancellationToken cancellationToken)
        {
            var httpClient = new HttpClient();

            var response = await httpClient.GetAsync($"http://api.weatherapi.com/v1/current.json?key=a66aaa298d964a68853145710233110&q={turnContext.Activity.Text}&aqi=no", cancellationToken);

            if (response.IsSuccessStatusCode)
            {
                string responseBody = await response.Content.ReadAsStringAsync(cancellationToken);
                var weather = JsonConvert.DeserializeObject<WeatherData>(responseBody);
                var replyText = MessageFactory.Text($"อากาศตอนนี้ที่ {weather!.Location!.Name} อุณหภูมิ {weather!.Current!.TempC} องศาเซลเซียส");
                await turnContext.SendActivityAsync(replyText, cancellationToken);
            } else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest) // 400
            {
                await turnContext.SendActivityAsync(MessageFactory.Text($"ไม่พบข้อมูลเมือง/ประเทศ {turnContext.Activity.Text} นี้"), cancellationToken);
            }

            await SendSuggestedActionsAsync(turnContext, cancellationToken);

        }


        // welcome text
        public static async Task HandleWelcomeAsync(IList<ChannelAccount> membersAdded, ITurnContext<IConversationUpdateActivity> turnContext, CancellationToken cancellationToken)
        {
            var welcomeText = "ยินดีต้อนรับ นี่คือ NT Bot Workshop 3 ตรวจสอบสภาพอากาศ!";
            foreach (var member in membersAdded)
            {
                if (member.Id != turnContext.Activity.Recipient.Id)
                {
                    await turnContext.SendActivityAsync(MessageFactory.Text(welcomeText, welcomeText), cancellationToken);

                    await SendSuggestedActionsAsync(turnContext, cancellationToken);
                }
            }
        }

        // send suggestedActions
        private static async Task SendSuggestedActionsAsync(ITurnContext turnContext, CancellationToken cancellationToken)
        {
            var reply = MessageFactory.Text("เลือกเมนูที่คุณสนใจ");

            reply.SuggestedActions = new SuggestedActions() {
            
                Actions = new List<CardAction>() { 
                    new CardAction() { Title = "กรุงเทพฯ", Value = "bangkok", Type = ActionTypes.ImBack, Image = "https://codingthailand.com/site/img/camera.png" },
                    new CardAction() { Title = "ลอนดอน", Value = "london" , Type = ActionTypes.ImBack, Image = "https://codingthailand.com/site/img/new.gif" },
                    new CardAction() { Title = "เว็บไซต์ของเรา", Value = "https://www.ntplc.co.th", Type = ActionTypes.OpenUrl },
                    new CardAction() { Title = "โทรหาเรา", Value = "tel:1888", Type = ActionTypes.Call },
                    new CardAction() { Title = "ดาวน์โหลดไฟล์", Value = "https://bit.ly/3lQ29C9", Type = ActionTypes.DownloadFile },
                    new CardAction() { Title = "ญี่ปุ่น", Value = "japan", Type = ActionTypes.PostBack }, //value จะมองไม่เห็นที่ห้อง chat
                }
            
            };
            await turnContext.SendActivityAsync(reply, cancellationToken);
        }

    }
}
