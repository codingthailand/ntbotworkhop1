﻿using Microsoft.Bot.Builder;
using Microsoft.Bot.Schema;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace NTBotWorkshop1.Bots
{
    public class WorkshopOne
    {
        public static async Task HandleActivityAsync(ITurnContext<IMessageActivity> turnContext, CancellationToken cancellationToken)
        {
            // var replyText = $"Echo: {turnContext.Activity.Text}";
            var replyText = turnContext.Activity.Text;

            if (replyText.Contains("สวัสดี") || replyText.Contains("hello") || replyText.Contains("hi"))
            {
                await turnContext.SendActivityAsync(MessageFactory.Text("สวัสดีครับคุณลูกค้า ต้องการให้ช่วยเหลือด้านใดครับ"), cancellationToken);
            }
            if (replyText.Contains("date"))
            {
                await turnContext.SendActivityAsync(MessageFactory.Text($"วันนี้วันที่ {DateTime.Now.ToShortDateString()}"), cancellationToken);
            }
            else
            {
                await turnContext.SendActivityAsync(MessageFactory.Text(replyText, replyText), cancellationToken);
            }
        }


        // welcome text
        public static async Task HandleWelcomeAsync(IList<ChannelAccount> membersAdded, ITurnContext<IConversationUpdateActivity> turnContext, CancellationToken cancellationToken)
        {
            var welcomeText = "ยินดีต้อนรับ นี่คือ NT Bot WorkshopOne!";
            foreach (var member in membersAdded)
            {
                if (member.Id != turnContext.Activity.Recipient.Id)
                {
                    await turnContext.SendActivityAsync(MessageFactory.Text(welcomeText, welcomeText), cancellationToken);
                }
            }
        }

    }
}
