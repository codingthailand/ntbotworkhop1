﻿using Microsoft.Bot.Builder;
using Microsoft.Bot.Schema;
using Newtonsoft.Json;
using NTBotWorkshop1.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text.Json.Nodes;
using System.Threading;
using System.Threading.Tasks;

namespace NTBotWorkshop1.Bots
{
    public class WorkshopFive
    {
        public static async Task HandleActivityAsync(ITurnContext<IMessageActivity> turnContext, CancellationToken cancellationToken)
        {

            if (turnContext.Activity.Text.ToLower().Contains("product"))
            {
                var httpClient = new HttpClient();

                var response = await httpClient.GetAsync("https://api.codingthailand.com/api/course", cancellationToken);

                if (response.IsSuccessStatusCode)
                {
                    string responseBody = await response.Content.ReadAsStringAsync(cancellationToken);
                    var product = JsonConvert.DeserializeObject<Product>(responseBody);

                    await DisplayProductAsync(product!, turnContext, cancellationToken);
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.NotFound) // 404
                {
                    await turnContext.SendActivityAsync(MessageFactory.Text($"ไม่พบ Url นี้ในระบบ"), cancellationToken);
                }
            }



        }


        // welcome text
        public static async Task HandleWelcomeAsync(IList<ChannelAccount> membersAdded, ITurnContext<IConversationUpdateActivity> turnContext, CancellationToken cancellationToken)
        {
            var welcomeText = "ยินดีต้อนรับ นี่คือ NT Bot Workshop 5 แสดงสินค้า";
            foreach (var member in membersAdded)
            {
                if (member.Id != turnContext.Activity.Recipient.Id)
                {
                    await turnContext.SendActivityAsync(MessageFactory.Text(welcomeText, welcomeText), cancellationToken);


                }
            }
        }

        // send DisplayProductAsync
        private static async Task DisplayProductAsync(Product product, ITurnContext turnContext, CancellationToken cancellationToken)
        {
            //Card Carousel

            //var heroCards = new List<ThumbnailCard>();
            var heroCards = new List<HeroCard>();
            foreach (var item in product.Data!)
            {
                //var heroCard = new ThumbnailCard()
                var heroCard = new HeroCard()
                {
                    Images = new List<CardImage> { new CardImage($"{item.Picture}") },
                    Title = $"{item.Title}",
                    Subtitle = $"{item.Date}",
                    Text = $"{item.Detail}",
                    Buttons = new List<CardAction> {
                        new CardAction(type: ActionTypes.ImBack, title: "ดูเพิ่มเติม", value: "https://codingthailand.com"),
                        new CardAction(type: ActionTypes.OpenUrl, title: "ซื้อเลย", value: "https://codingthailand.com"),
                    }
                };
                heroCards.Add(heroCard);
            }

            var attachments = new List<Attachment>();
            foreach (var card in heroCards)
            {
                attachments.Add(card.ToAttachment());
            }

            var reply = MessageFactory.Carousel(attachments);

            await turnContext.SendActivityAsync(reply, cancellationToken);
        }

    }
}
