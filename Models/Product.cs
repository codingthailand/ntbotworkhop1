﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace NTBotWorkshop1.Models
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
    public class Datum
    {
        [JsonProperty("id")]
        public int? Id { get; set; }

        [JsonProperty("title")]
        public string? Title { get; set; }

        [JsonProperty("detail")]
        public string? Detail { get; set; }

        [JsonProperty("date")]
        public string? Date { get; set; }

        [JsonProperty("view")]
        public int? View { get; set; }

        [JsonProperty("picture")]
        public string? Picture { get; set; }
    }

    public class Meta
    {
        [JsonProperty("status")]
        public string? Status { get; set; }

        [JsonProperty("status_code")]
        public int? StatusCode { get; set; }
    }

    public class Product
    {
        [JsonProperty("data")]
        public List<Datum>? Data { get; set; }

        [JsonProperty("meta")]
        public Meta? Meta { get; set; }
    }


}
