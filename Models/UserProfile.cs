﻿namespace NTBotWorkshop1.Models
{
    public class UserProfile
    {
        public string? Name { get; set; }
        public string? UserId { get; set; }
    }
}
