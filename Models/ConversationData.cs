﻿namespace NTBotWorkshop1.Models
{
    public class ConversationData
    {

        public enum Question
        {
            UserId,
            ToolName,
            ToolDescription,
            None
        }

        public Question LastQuestionAsked { get; set; } = Question.None;

        // The time-stamp of the most recent incoming message.
        public string? Timestamp { get; set; }

        // The ID of the user's channel.
        public string? ChannelId { get; set; }

        // Track whether we have already asked the user's name
        public bool PromptedUserForName { get; set; } = false;


        public string? ToolName { get; set;}
        public string? ToolDescription { get; set;}
    }
}
